import time
import string
import random

from selenium.webdriver import Chrome
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager


def get_random_string(length):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))

random_string = get_random_string(5)


# Test - uruchomienie Chroma
def test_ui_demo_testarena():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony testareny
    browser.get('http://demo.testarena.pl/zaloguj')

    # wpisanie loginu
    browser.find_element(By.CSS_SELECTOR, '#email').send_keys('administrator@testarena.pl')

    # wpisanie hasła
    browser.find_element(By.CSS_SELECTOR, '#password').send_keys('sumXQQ72$L')

    # kliknięcie zaloguj
    browser.find_element(By.CSS_SELECTOR, '#login').click()

    # otwarcie panelu administratora
    browser.find_element(By.CSS_SELECTOR, '[title=Administracja]').click()

    # kliknięcie dodaj projekt
    browser.find_element(By.CSS_SELECTOR, '.button_link').click()

    # wpisanie nazwy projektu
    browser.find_element(By.CSS_SELECTOR, '#name').send_keys(random_string)

    # wpisanie prefixu
    browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(random_string)

    time.sleep(2)
    # klinięcie zapisz
    browser.find_element(By.CSS_SELECTOR, '#save').click()

    # wejście do projektów
    browser.find_element(By.CSS_SELECTOR, '.activeMenu').click()

    # wyszukanie projektu
    browser.find_element(By.CSS_SELECTOR, '#search').send_keys(random_string)

    # kliknięcie wyszukaj
    browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()

    #asercja wyszukania

    searching_result = browser.find_element(By.LINK_TEXT, random_string)
    assert searching_result.is_displayed() is True

    time.sleep(5)

    # Zamknięcie przeglądarki
    browser.quit()
